﻿using UnityEngine;

public class CustomEventTrigger : MonoBehaviour
{
    private Manager manager;
    
    private void Awake()
    {
        manager = Manager.Instance;
    }

    private void Start()
    {

    }


    public void OnPointerExited(GameObject _gameObject)
    {
        manager.PointerIsExited(_gameObject);
    }

    public void OnPointerEntered(GameObject _gameObject)
    {
        manager.PointerIsEntered(_gameObject);
    }

    public void OnPointerDowned(GameObject _gameObject)
    {
        manager.PointerIsDowned(_gameObject);
    }

    public void OnPointerUped(GameObject _gameObject)
    {
        manager.PointerIsUped(_gameObject);
    }

    public void OnPointerClicked(GameObject _gameObject)
    {
        manager.PointerIsClicked(_gameObject);
    }
}
