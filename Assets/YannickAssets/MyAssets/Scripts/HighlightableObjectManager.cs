﻿using UnityEngine;

public class HighlightableObjectManager : MonoBehaviour
{
    private Manager manager;

    private void Awake()
    {
        manager = Manager.Instance;
    }

    private void OnEnable()
    {
        manager.OnPointerEntered += OnPointerEntered;
        manager.OnPointerClicked += OnPointerClicked;
        manager.OnPointerExited += OnPointerExited;
        manager.OnPointerDowned += OnPointerDowned;
        manager.OnPointerUped += OnPointerUped;
    }


    private void OnDisable()
    {
        manager.OnPointerEntered -= OnPointerEntered;
        manager.OnPointerClicked -= OnPointerClicked;
        manager.OnPointerExited -= OnPointerExited;
        manager.OnPointerDowned -= OnPointerDowned;
        manager.OnPointerUped -= OnPointerUped;
    }

    private void OnPointerEntered(object sender, Manager.PointerEvent pe)
    {
        var highlightableItem = pe.m_Gameobject.GetComponent<IHighLightable>();
        if (highlightableItem != null) {
            highlightableItem.HighLight();
        }
       
    }

    private void OnPointerDowned(object sender, Manager.PointerEvent pe)
    {

    }

    private void OnPointerClicked(object sender, Manager.PointerEvent pe)
    {
        var animableItem = pe.m_Gameobject.GetComponent<IAnimable>();
        if (animableItem != null) {
            animableItem.Animate();
        }

    }

    private void OnPointerUped(object sender, Manager.PointerEvent pe)
    {
       // Debug.Log("upppp");
    }

    public void DestroyCurrentObject()
    {
        //Debug.Log("Call this function");
    }


    private void OnPointerExited(object sender, Manager.PointerEvent pe)
    {
        var highlightableItem = pe.m_Gameobject.GetComponent<IHighLightable>();

        if (highlightableItem != null) {
            highlightableItem.HighLight();
        }

    }

}
