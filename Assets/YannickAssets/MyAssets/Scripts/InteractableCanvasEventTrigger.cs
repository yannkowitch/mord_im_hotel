﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableCanvasEventTrigger : MonoBehaviour
{
    [SerializeField] private bool isEnabled = true;

    public bool IsEnabled { get => isEnabled; set => isEnabled = value; }
}
