﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Utils;
public class Manager : Singleton<Manager>
{
    #region events
    public event EventHandler<PointerEvent> OnPointerExited, OnPointerEntered, OnPointerDowned, OnPointerUped, OnPointerClicked;
    public event EventHandler<PlayerTriggerEvent> OnPlayerTriggerEnter;
    #endregion

    bool showDebug = false;

    #region FIRE EVENTS

    public void PlayerTriggerIsEntered(PlayerCollision playerCollision)
    {
        if (OnPlayerTriggerEnter != null)
        {
            OnPlayerTriggerEnter(this, new PlayerTriggerEvent(playerCollision.otherCollider));

            Debug.Log(playerCollision.otherCollider.name);
        }
    }


    public void PointerIsClicked(GameObject gameObject)
    {
        if (OnPointerClicked != null)
        {
            PointerEvent pointerEvent = new PointerEvent(gameObject);
            OnPointerClicked(this, pointerEvent);
            if (showDebug)
                Debug.Log("Pointer is clicked : " + pointerEvent.m_Gameobject);
        }

    }

    public void PointerIsEntered(GameObject gameObject)
    {
        if (OnPointerEntered != null)
        {
            PointerEvent pointerEvent = new PointerEvent(gameObject);

            OnPointerEntered(this, pointerEvent);
            if (showDebug)
                Debug.Log("Pointer is entered : " + pointerEvent.m_Gameobject);
        }
    }

    public void PointerIsDowned(GameObject gameObject)
    {
        if (OnPointerDowned != null)
        {
            PointerEvent pointerEvent = new PointerEvent(gameObject);

            OnPointerDowned(this, pointerEvent);
            if (showDebug)
                Debug.Log("Pointer is downed : " + pointerEvent.m_Gameobject);
        }
    }

    public void PointerIsUped(GameObject gameObject)
    {
        if (OnPointerUped != null)
        {
            PointerEvent pointerEvent = new PointerEvent(gameObject);

            OnPointerUped(this, pointerEvent);
            if (showDebug)
                Debug.Log("Pointer is uped : " + pointerEvent.m_Gameobject);
        }
    }

    public void PointerIsExited(GameObject gameObject)
    {
        if (OnPointerExited != null)
        {
            PointerEvent pointerEvent = new PointerEvent(gameObject);
            OnPointerExited(this, pointerEvent);
            if (showDebug)
                Debug.Log("Pointer is exited : " + pointerEvent.m_Gameobject);
        }
    }
    #endregion

    #region event classes
    public class PointerEvent : EventArgs
    {
        public GameObject m_Gameobject { get; private set; }

        public PointerEvent(GameObject m_Gameobject)
        {

            this.m_Gameobject = m_Gameobject;
        }
    }

    public class PlayerTriggerEvent : EventArgs
    {
        public Collider otherCollider { get; private set; }

        public PlayerTriggerEvent(Collider otherCollider)
        {
            this.otherCollider = otherCollider;
        }
    }

    #endregion


    public delegate void PointerEventHandler(GameObject m_Gameobject);
    public static PointerEventHandler _OnPointerExited;
    public static PointerEventHandler _OnPointerEntered;
    public static PointerEventHandler _OnPointerDowned;
    public static PointerEventHandler _OnPointerUped;
    public static PointerEventHandler _OnPointerClicked;



    public void OnPointerExit()
    {
        if (_OnPointerExited != null)
            _OnPointerExited(this.gameObject);
    }

    public void OnPointerEnter()
    {
        if (_OnPointerEntered != null)
            _OnPointerEntered(this.gameObject);
    }

    public void OnPointerDown()
    {
        if (_OnPointerDowned != null)
            _OnPointerDowned(this.gameObject);
    }

    public void OnPointerUp()
    {
        if (_OnPointerUped != null)
            _OnPointerUped(this.gameObject);
    }
    public void OnPointerClick()
    {
        if (_OnPointerClicked != null)
            _OnPointerClicked(this.gameObject);
    }


}
