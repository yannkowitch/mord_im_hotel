﻿using UnityEngine;
using System;
using UnityEngine.UI;

[RequireComponent(typeof(Timer))]
public class InteractableCanvasManager : MonoBehaviour
{
    [SerializeField] DummyInteractableCanvas _userActionCanvas = null;
    private Manager manager;
    private Timer timer = null;

    private void Awake()
    {
        manager = Manager.Instance;
        if (_userActionCanvas == null) {
            throw new NullReferenceException(_userActionCanvas.name + " is not set to an instance");
        }

        timer = GetComponent<Timer>();
    }


    void Start()
    {
    }

    private void OnEnable()
    {
        manager.OnPointerClicked += HandleOnGameeobjectPointerClicked;
        manager.OnPointerExited += HandleOnGameobjectPointerExited;
        timer.OnTimerEnded += HandleOnTimerEnded;
        _userActionCanvas.OnDummyButton1Clicked += HandleOnButton1Clicked;
        _userActionCanvas.OnDummyButton2Clicked += HandleOnButton2Clicked;
    }


    private void OnDisable()
    {
        manager.OnPointerClicked -= HandleOnGameeobjectPointerClicked;
        manager.OnPointerExited -= HandleOnGameobjectPointerExited;
        timer.OnTimerEnded -= HandleOnTimerEnded;
        _userActionCanvas.OnDummyButton1Clicked -= HandleOnButton1Clicked;
        _userActionCanvas.OnDummyButton2Clicked -= HandleOnButton2Clicked;

    }

    private void HandleOnButton1Clicked(Button button)
    {
        _userActionCanvas.TypeText( button.name);
    }
    private void HandleOnButton2Clicked(Button button)
    {
        _userActionCanvas.TypeText(button.name);
    }

    private void HandleOnTimerEnded()
    {
        _userActionCanvas.Close();
        timer.ResetTimer();
    }

    private void HandleOnGameobjectPointerExited(object sender, Manager.PointerEvent e)
    {

        var interactableCanvasEventTrigger = e.m_Gameobject.GetComponent<InteractableCanvasEventTrigger>();
        if (interactableCanvasEventTrigger == null) {
            return;
        }
        if (interactableCanvasEventTrigger.IsEnabled) {

            timer.ResetTimer()
                 .StartTimer();
        }

    }


    private void HandleOnGameeobjectPointerClicked(object sender, Manager.PointerEvent e)
    {
        var interactableCanvasEventTrigger = e.m_Gameobject.GetComponent<InteractableCanvasEventTrigger>();
        if (interactableCanvasEventTrigger == null) {
            return;
        }
        if (interactableCanvasEventTrigger.IsEnabled) {
            _userActionCanvas.Open();
            _userActionCanvas.TypeText(e.m_Gameobject.name);

        }

    }


    public void OnCanvasPointerEnter(GameObject m_Gameobject)
    {
        timer.ResetTimer();
    }

    public void OnCanvasPointerExit(GameObject m_Gameobject)
    {
        timer.ResetTimer()
             .StartTimer();
    }




}
