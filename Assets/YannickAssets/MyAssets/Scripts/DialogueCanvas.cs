﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;

public class DialogueCanvas : MonoBehaviour
{
    [SerializeField] Animator animator;
    [SerializeField] TextMeshProUGUI textMeshProUGUI;
    [SerializeField] public string[] _sentences;


    public delegate void SentenceAtTheEndEventHandler();
    public static SentenceAtTheEndEventHandler OnSentenceAtTheEnd;

    protected Coroutine m_DeactivationCoroutine;
    private Coroutine m_TextTypingCoroutine = null;

    protected readonly int m_HashActivePara = Animator.StringToHash("Active");

    public Action OnTextMaxLengthReached;


    public void TypeText(string sentence)
    {
        if (m_TextTypingCoroutine != null) {
            StopCoroutine(m_TextTypingCoroutine);
            m_TextTypingCoroutine = null;
        }

        m_TextTypingCoroutine = StartCoroutine(TypeTextIE(sentence));
    }

    public DialogueCanvas Open()
    {
        if (m_DeactivationCoroutine != null) {
            StopCoroutine(m_DeactivationCoroutine);
            m_DeactivationCoroutine = null;
        }

        gameObject.SetActive(true);
        animator.SetBool(m_HashActivePara, true);
        return this;

    }

    public void CloseAfter(float seconds)
    {
        m_DeactivationCoroutine = StartCoroutine(SetAnimatorParameterWithDelay(seconds));
    }

    private IEnumerator TypeTextIE(string sentence)
    {

        textMeshProUGUI.text = "";
        yield return new WaitForSeconds(0.25f);

        foreach (char letter in sentence.ToCharArray()) {
            textMeshProUGUI.text += letter;

            if (textMeshProUGUI.text.Length == sentence.Length) {
                OnTextMaxLengthReached?.Invoke();

            }
            // wait a single frame
            yield return null;
        }
    }

    private IEnumerator SetAnimatorParameterWithDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        animator.SetBool(m_HashActivePara, false);
    }
}
