﻿using UnityEngine;
using System;

public class DialogueCanvasManager : MonoBehaviour
{
    private bool _isDoorClicked;

    [SerializeField] DialogueCanvas _dialogueCanvas = null;
    private Manager manager;

    private void Awake()
    {
        manager = Manager.Instance;
        if (_dialogueCanvas == null) {
            throw new NullReferenceException(_dialogueCanvas.name + " is not set to an instance");
        }
    }


    private void OnEnable()
    {
        manager.OnPointerEntered += OnPointerEntered;
        manager.OnPointerClicked += OnPointerClicked;
        manager.OnPointerExited += OnPointerExited;
        manager.OnPlayerTriggerEnter += OnPlayerTriggerEnter;
        _dialogueCanvas.OnTextMaxLengthReached += HandleOnTextMaxLengthReached;

    }
    private void OnDisable()
    {
        manager.OnPointerEntered -= OnPointerEntered;
        manager.OnPointerClicked -= OnPointerClicked;
        manager.OnPointerExited -= OnPointerExited;
        manager.OnPlayerTriggerEnter -= OnPlayerTriggerEnter;
        _dialogueCanvas.OnTextMaxLengthReached -= HandleOnTextMaxLengthReached;
    }

    private void HandleOnTextMaxLengthReached()
    {
        if (_isDoorClicked) {
            _dialogueCanvas.CloseAfter(0.25f);
        }
    }

    private void OnPlayerTriggerEnter(object sender, Manager.PlayerTriggerEvent e)
    {
        if (e.otherCollider.gameObject.tag.Equals("bedRoom")) {
            _dialogueCanvas.Open().
                           TypeText(_dialogueCanvas._sentences[2] + e.otherCollider.gameObject.tag);
        }
        else if (e.otherCollider.gameObject.tag.Equals("bathRoom")) {
            _dialogueCanvas.Open()
                           .TypeText(_dialogueCanvas._sentences[3] + e.otherCollider.gameObject.tag);
        }

    }

    private void OnPointerExited(object sender, Manager.PointerEvent e)
    {
        if (e.m_Gameobject.GetComponent<DoorAnimationBehavior>() != null) {
            _dialogueCanvas.CloseAfter(0.25f);
        }
    }

    private void OnPointerClicked(object sender, Manager.PointerEvent e)
    {
        if (e.m_Gameobject.GetComponent<DoorAnimationBehavior>() != null) {
            _isDoorClicked = true;
            _dialogueCanvas.TypeText(_dialogueCanvas._sentences[1]);
            _isDoorClicked = false;
        }
    }

    private void OnPointerEntered(object sender, Manager.PointerEvent e)
    {
        DoorAnimationBehavior door = e.m_Gameobject.GetComponent<DoorAnimationBehavior>();

        if (door != null) {
            if (door.IsDoorOpened)
                return;
            _dialogueCanvas.Open()
                           .TypeText(_dialogueCanvas._sentences[0]);

        }

    }

}
