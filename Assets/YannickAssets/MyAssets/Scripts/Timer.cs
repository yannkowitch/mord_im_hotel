﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    [SerializeField] private float maxTime = 4;
    private float timeRemaining = 0;
    public Action OnTimerEnded;

    public bool IsTimerRunning { get; private set; } = false;

    private void Start()
    {
        ResetTimer();
    }

    public void StartTimer()
    {
        IsTimerRunning = true;
        Debug.Log("Timer is Started!");
    }

    public Timer ResetTimer()
    {
        IsTimerRunning = false;
        timeRemaining = maxTime;
        Debug.Log("Timer is Reset!");
        return this;
    }
   
    private void Update()
    {
        if (IsTimerRunning) {
            Run();
        }
    }

    private void Run()
    {
        if (timeRemaining > 0) {
            timeRemaining -= Time.deltaTime;
            //Debug.Log("Timer is running!");
        }
        else {
            Debug.Log("Time has run out!");
            timeRemaining = 0;
            IsTimerRunning = false;
            OnTimerEnded?.Invoke();
        }
    }
}
