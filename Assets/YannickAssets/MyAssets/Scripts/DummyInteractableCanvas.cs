﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DummyInteractableCanvas : MonoBehaviour
{
    [SerializeField] private Animator animator = null;
    [SerializeField] private TextMeshProUGUI statusText = null;
    [SerializeField] private Button dummyButton1 = null;
    [SerializeField] private Button dummyButton2 = null;
    [SerializeField] private Toggle dummyToogle1 = null;
    [SerializeField] private Toggle dummyToogle2 = null;

    private Coroutine m_TextTypingCoroutine = null;
    private readonly int m_HashActivePara = Animator.StringToHash("Active");

    public Action<Button> OnDummyButton1Clicked;
    public Action<Button> OnDummyButton2Clicked;

    public TextMeshProUGUI StatusText { get => statusText; set => statusText = value; }

    public void OnDummyButton1Click(Button btn)
    {
        OnDummyButton1Clicked?.Invoke(btn);
        Debug.Log(btn.name);
    }
    public void OnDummyButton2Click(Button btn)
    {
        OnDummyButton2Clicked?.Invoke(btn);
        Debug.Log(btn.name);
    }
    public void Open()
    {
        animator.SetBool(m_HashActivePara, true);
    }

    public void Close()
    {
        animator.SetBool(m_HashActivePara, false);
    }


    public void TypeText(string sentence)
    {
        if (m_TextTypingCoroutine != null) {
            StopCoroutine(m_TextTypingCoroutine);
            m_TextTypingCoroutine = null;
        }

        m_TextTypingCoroutine = StartCoroutine(TypeTextIE(sentence));
    }


    private IEnumerator TypeTextIE(string sentence)
    {
        StatusText.text = "";
        yield return new WaitForSeconds(0.25f);

        foreach (char letter in sentence.ToCharArray()) {
            StatusText.text += letter;
            // wait a single frame
            yield return null;
        }
    }


}
