﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionManager : MonoBehaviour
{
    BlackMaskBehaviour mBlackMask;

    float mTransitionCursor;
    float mCurrentTime;
    bool mPlaying;
    bool mBackward;


    [Range(0.1f, 15.0f)]
    public float transitionDuration = 1.5f; // seconds

    public delegate void ScreenEventHandler();
    public static event ScreenEventHandler OnScreenFullyBlack;

    // Use this for initialization
    void Start()
    {
        mBlackMask = FindObjectOfType<BlackMaskBehaviour>();

        SetBlackMaskVisible(false, 0);

        //  Play(false);
    }
    private void OnEnable()
    {
        Manager._OnPointerClicked += OnPointerCliked;
    }

    private void OnDisable()
    {
        Manager._OnPointerClicked -= OnPointerCliked;
    }

    private void OnPointerCliked(GameObject gameObject)
    {
     //   Debug.Log(gameObject.transform.parent.tag);
        if (gameObject.transform.parent.tag == "floor")
        {
            Play(false);

        }

    }

    public void Play(bool reverse)
    {
        // dont' restart playing during a transition
        if (!mPlaying)
        {
            mPlaying = true;
            mBackward = reverse;
            mTransitionCursor = mBackward ? 1 : 0;
            mTransitionCursor = 0;

        }
    }


    // Update is called once per frame
    void Update()
    {
        //  SetBlackMaskVisible(true, 1);
        float time = Time.realtimeSinceStartup;
        float deltaTime = Mathf.Clamp01(time - mCurrentTime);
        mCurrentTime = time;

        if (mPlaying)
        {
            float fadeFactor = 0;

            if (mTransitionCursor < 0.33f)
            {
                // fade to full black in first part of transition
                fadeFactor = Mathf.SmoothStep(0, 1, mTransitionCursor / 0.33f);
            }
            else if (mTransitionCursor < 0.66f)
            {
                // between 33% and 66% we stay in full black
                fadeFactor = 1;
   //             Debug.Log("black screen");

                if (OnScreenFullyBlack != null)
                {
                    OnScreenFullyBlack();
                }

            }
            else if (mTransitionCursor > 0.66f)// > 0.66
            {
                // between 66% and 100% we fade out
                fadeFactor = Mathf.SmoothStep(1, 0, (mTransitionCursor - 0.66f) / 0.33f);
            }

            //       Debug.Log("Fade factor" + fadeFactor);
            SetBlackMaskVisible(true, fadeFactor);

            float delta = (mBackward ? -1 : 1) * deltaTime / transitionDuration;

            mTransitionCursor += delta;
            //       Debug.Log(mTransitionCursor);

            if (mTransitionCursor <= 0 || mTransitionCursor >= 1)
            {
                // Done: stop animated transition
                mTransitionCursor = Mathf.Clamp01(mTransitionCursor);
                mPlaying = false;

                SetBlackMaskVisible(false, 0);
            }

        }


    }

    void SetBlackMaskVisible(bool visible, float fadeFactor)
    {
        if (mBlackMask)
        {
            mBlackMask.GetComponent<Renderer>().enabled = visible;
            mBlackMask.SetFadeFactor(fadeFactor);
        }
    }
}
