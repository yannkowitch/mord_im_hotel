﻿using UnityEngine;

public class TeleportableObjectManager : MonoBehaviour
{
    Manager manager;
    private void Awake()
    {
        manager = Manager.Instance;
    }
    private void OnEnable()
    {

        manager.OnPointerClicked += OnPointerCliked;
    }

    private void OnDisable()
    {

        manager.OnPointerClicked -= OnPointerCliked;
    }

    private void OnPointerCliked(object sender, Manager.PointerEvent e)
    {
        var tile = e.m_Gameobject.GetComponent<TileScript>();
        if (tile == null) {
            return;
        }

        ITeleportable teleportableItem = GameObject.FindGameObjectWithTag("Player").GetComponent<ITeleportable>();

        if (teleportableItem == null) {
            return;
        }
        teleportableItem.TeleportTo(tile.GetPosition());

    }

}
