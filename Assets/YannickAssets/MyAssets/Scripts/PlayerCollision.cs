﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    public Collider otherCollider { get; private set; }
    Manager manager;
    private void Awake()
    {
     
        manager = Manager.Instance;
        otherCollider = null;
    }

    private void OnTriggerEnter(Collider otherCollider)
    {
        this.otherCollider = otherCollider;
        manager.PlayerTriggerIsEntered(this);
      
        
    }
}
