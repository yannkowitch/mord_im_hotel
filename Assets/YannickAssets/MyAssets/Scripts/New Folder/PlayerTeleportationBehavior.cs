﻿
using System;
using UnityEngine;

public class PlayerTeleportationBehavior : MonoBehaviour, ITeleportable
{
    private Vector3 playerEndPosition;
    private float playerYPos;
    
    private void Start()
    {
        playerYPos = this.gameObject.transform.position.y;
    }

   

    public void TeleportTo(Vector3 position)
    {
        playerEndPosition = position;
        playerEndPosition.y = playerYPos;

        this.transform.position = playerEndPosition;
    }
}
