﻿using System;
using UnityEngine;

public class DoorAnimationBehavior : MonoBehaviour, IAnimable
{
    [SerializeField] Animator _animator = null;
    public static Action<bool> OnDoorIsOpening;

    public bool IsDoorOpened { get; private set; }

    private void Awake()
    {
        if (_animator == null) {
            throw new NullReferenceException(_animator.name + "is not pointing to an object");
        }
    }


    public void OpenDoor()
    {
        _animator.SetBool("openDoor", true);
        IsDoorOpened = true;
        OnDoorIsOpening?.Invoke(IsDoorOpened);

    }

    public void Animate()
    {
        if (!IsDoorOpened) {

            OpenDoor();
        }
    }
}
