﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorHighLightBehavior : Highlighter, IHighLightable
{
    private DoorAnimationBehavior doorAnimationScript = null;
    protected new void Start()
    {
        base.Start();
        doorAnimationScript = GetComponent<DoorAnimationBehavior>();
        if (doorAnimationScript == null) {
            throw new NullReferenceException(doorAnimationScript.name + "is not set to an instance");
        }
    }
    public void HighLight()
    {

        if (doorAnimationScript.IsDoorOpened == false) {
            if (isHighlighted == false) {
                Highlight();
            }
            else {
                UnHighlight();
            }
        }
        else {
            UnHighlight();
        }


    }
}
