﻿using System;
using UnityEngine;

public class Highlighter : MonoBehaviour
{
    [SerializeField] protected Material _highlightedMaterial;
    protected Material _defaultMaterial = null;
    protected Renderer _renderer = null;
    protected bool isHighlighted = false;

    protected void Awake()
    {

        if (_highlightedMaterial == null) {
            throw new NullReferenceException(_highlightedMaterial.name + "is not set to an instance");
        }
    }
    protected void Start()
    {
        _renderer = GetComponent<Renderer>();
        _defaultMaterial = _renderer.material;
        if (_defaultMaterial == null) {
            throw new NullReferenceException(_defaultMaterial.name + "is not set to an instance");
        }

    }

    public void Highlight()
    {
        _renderer.material = _highlightedMaterial;
        isHighlighted = true;
    }

    public void UnHighlight()
    {
        _renderer.material = _defaultMaterial;
        isHighlighted = false;
    }
}
