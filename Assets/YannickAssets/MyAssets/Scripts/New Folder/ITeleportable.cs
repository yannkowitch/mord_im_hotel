﻿using UnityEngine;

public interface ITeleportable
{
    void TeleportTo(Vector3 position);

}
