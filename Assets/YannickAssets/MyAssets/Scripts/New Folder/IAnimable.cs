﻿public interface IAnimable
{
    void Animate();
}
